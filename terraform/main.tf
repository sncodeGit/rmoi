terraform {
  required_providers {
    selectel = {
      source = "selectel/selectel"
      version = "3.4.0"
    }
  }
}

# Initialize Selectel provider with token.
provider "selectel" {
  token = "${var.sel_token}"
}

# Create an OpenStack Compute instance.
module "server_remote_root_disk" {
  source = "./terraform-examples/modules/vpc/server_remote_root_disk"

  # OpenStack auth.
  os_project_name  = "${var.project_name}"
  os_user_name     = "${var.user_name}"
  os_user_password = "${var.user_password}"
  os_domain_name   = "${var.sel_account}"
  os_auth_url      = "https://api.selvpc.ru/identity/v3"
  os_region        = "ru-7"

  # OpenStack Instance parameters.
  server_name         = "rmoi"
  server_zone         = "ru-7a"
  server_vcpus        = 2
  server_ram_mb       = 2048
  server_root_disk_gb = 100
  server_volume_type  = "fast.ru-7a"
  server_image_name   = "Ubuntu 18.04 LTS 64-bit"
  server_ssh_key      = file("~/.ssh/id_rsa.pub")
  server_ssh_key_user = "${var.user_id}"
}