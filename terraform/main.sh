#!/bin/bash

# Init
terraform init

# Get req plan
terraform plan -var-file='terraform.tfvars'

# Apply plan
terraform apply -var-file='terraform.tfvars'

# Destroy all resources
terraform destroy -var-file='terraform.tfvars'
