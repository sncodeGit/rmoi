#!/bin/bash

# Install Nginx
ansible-playbook -i inventory main.yml --tags 'nginx'

# Add index.html
ansible-playbook -i inventory main.yml --tags 'site'