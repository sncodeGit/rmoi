#!/bin/bash

# Minikube - инструмент для запуска одноузлового кластера Kubernetes
# на виртуальной машине в персональном компьютере
minikube start

# kubectl - утилита для взаимодействия с k8s API
kubectl get nodes

#
kubectl create -f python-server.pod.yml

#
kubectl get pods

#
kubectl describe nodes

#
kubectl port-forward python-server 8080:8080

#
kubectl delete pod python-server
