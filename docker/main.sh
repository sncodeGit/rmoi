#!/bin/bash

# See python server
env \
HOST_NAME='0.0.0.0' \
SERVER_PORT='8080' \
BODY_CONTENT='Hello from python server!' \
python3 python-server/main.py

# Build image
docker build --tag python-server:0.1 ./python-server

# Run container from prev image
docker run -itd --name python-server -p 127.0.0.1:8080:8080 python-server:0.1

# Stop container
docker container stop python-server

# Remove container
docker container rm python-server

# Remove image
docker image rm python-server:0.1
